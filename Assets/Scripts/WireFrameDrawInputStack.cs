﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireFrameDrawInputStack
{
    private int _visibleMin;
    private int _visibleMax;
    private int _visibleAdded;
    private int _visibleShown;

    public WireFrameDrawInputStack(int visibleMin, int visibleMax)
    {
        _visibleMin = visibleMin;
        _visibleMax = visibleMax;

        _visibleShown = _visibleMin;
        _visibleAdded = _visibleMin;
    }

    public int TrianglesToShowCount { get { return _visibleShown; } }

    public void Add()
    {
        _visibleShown++;
        _visibleShown = Mathf.Min(_visibleShown, _visibleMax);
        _visibleAdded = _visibleShown;
    }

    public void Do()
    {
        if (_visibleShown < _visibleAdded)
            _visibleShown++;
    }

    public void Redo()
    {
        _visibleShown--;
        _visibleShown = Mathf.Max(_visibleShown, _visibleMin);
    }
}
