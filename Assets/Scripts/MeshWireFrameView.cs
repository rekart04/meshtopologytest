﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshWireFrameView
{
    private class TriangleView
    {   
        private List<int>   _lineIndices = new List<int>();

        public List<int>    Indices     { get { return _lineIndices; } }

        public bool         IsEmpty     { get { return _lineIndices.Count == 0; } }
        
        public void AddEdge(int indexFirst, int indexSecond)
        {
            _lineIndices.Add(indexFirst);
            _lineIndices.Add(indexSecond);
        }
    }

    private List<TriangleView>  _wireFrameView;

    public MeshWireFrameView(Mesh mesh)
    {
        _wireFrameView      = new List<TriangleView>();
        var vertices        = mesh.vertices;
        var sourceIndices   = mesh.GetIndices(0);
        int triangleCount   = sourceIndices.Length / 3;

        int[] triangleIndices = new int[3];
        for (int i = 0; i < triangleCount; i++)
        {
            //fill indices for the following triangle
            for(int j = 0; j < 3; j++) {
                triangleIndices[j] = sourceIndices[i * 3 + j];
            }

            //put indices into view
            var triangleView = new TriangleView();
            for (int j = 0; j < 3; j++)
            {
                int indexFirst  = triangleIndices[j % 3];
                int indexSecond = triangleIndices[(j + 1) % 3];

                //edges in view must be unique
                if (IsWireFrameViewContainsEdge(ref vertices, indexFirst, indexSecond))
                    continue;

                triangleView.AddEdge(indexFirst, indexSecond);
            }

            //some triangles may be already visible due to other triangles edges drawn
            if(!triangleView.IsEmpty)
                _wireFrameView.Add(triangleView);
        }
    }

    public int UniqueTriangleCount { get { return _wireFrameView.Count; } }

    public List<int> GetMeshIndices(int triangleCount)
    {
        Debug.Assert(triangleCount <= UniqueTriangleCount);

        var result = new List<int>();
        for (int i = 0; i < triangleCount; i++) {
            result.AddRange(_wireFrameView[i].Indices);
        }

        return result;
    }

    private bool IsWireFrameViewContainsEdge(ref Vector3[] vertices, int indexFirst, int indexSecond)
    {
        for (int i = 0; i < _wireFrameView.Count; i++)
        {
            var triangleView = _wireFrameView[i];
            if (IsTriangleViewContainsEdge(triangleView, ref vertices, indexFirst, indexSecond))
                return true;
        }

        return false;
    }

    private bool IsTriangleViewContainsEdge(TriangleView view, ref Vector3[] vertices, int indexFirst, int indexSecond)
    {
        Debug.Assert(indexFirst != indexSecond);

        var indices     = view.Indices;
        int lineCount   = indices.Count / 2;
        for (int i = 0; i < lineCount; i++)
        {
            int existingIndexFirst = indices[i * 2];
            int existingIndexSecond = indices[i * 2 + 1];

            if (vertices[existingIndexFirst] == vertices[indexFirst] &&
                vertices[existingIndexSecond] == vertices[indexSecond])
                return true;

            if (vertices[existingIndexFirst] == vertices[indexSecond] &&
                vertices[existingIndexSecond] == vertices[indexFirst])
                return true;
        }

        return false;
    }
}
