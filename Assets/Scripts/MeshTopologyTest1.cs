﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTopologyTest1 : MonoBehaviour
{
    private Mesh                    _mesh;
    private MeshWireFrameView       _view;    
    private WireFrameDrawInputStack _inputStack;

    private void Awake()
    {
        _mesh       = GetComponent<MeshFilter>().mesh;
        _view       = new MeshWireFrameView(_mesh);
        _inputStack = new WireFrameDrawInputStack(1, _view.UniqueTriangleCount);
        RedrawWireFrame();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            _inputStack.Add();
            RedrawWireFrame();
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            _inputStack.Do();
            RedrawWireFrame();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            _inputStack.Redo();
            RedrawWireFrame();
        }
    }

    private void RedrawWireFrame()
    {
        var indices = _view.GetMeshIndices(_inputStack.TrianglesToShowCount);
        _mesh.SetIndices(indices, MeshTopology.Lines, 0);
    }
}
